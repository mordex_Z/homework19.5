﻿#include <iostream>
class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "some voice \n";
    }
};
class Dog : public Animal
{
public:
    void Voice() override
    {
        std::cout << "woof-woof! \n";
    }
};
class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "meow-meow! \n";
    }
};
class Monkey : public Animal
{
public:
    void Voice() override
    {
        std::cout << "cheek-cheek! :D \n";
    }
};

int main()
{
    Animal* array[4];

    Dog* dog = new Dog;
    Monkey* monkey1 = new Monkey;
    Cat* cat = new Cat;
    Monkey* monkey2 = new Monkey;

    array[0] = dog;
    array[1] = monkey1;
    array[2] = cat;
    array[3] = monkey2;

    for (int i = 0; i < 4; i++)
    {
        array[i]->Voice();
    }
}

